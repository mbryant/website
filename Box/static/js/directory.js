function formatSize(bytes) {
	const suffix = ["B", "KB", "MB", "GB", "TB", "PB"];

	for (let i = 0; i < suffix.length; i++) {
		if (bytes < 1024) {
			return "" + bytes.toFixed(1) + suffix[i];
		}

		bytes /= 1024;
	}

	return "Massive";
}

class NginxDirectory {
	constructor(directoryName, variant) {
		this.root = "https://box.programsareproofs.com/" + variant;
		this.dir = document.getElementById(directoryName);
		this.load("");
	}

	load(path) {
		let req = new XMLHttpRequest();
		req.addEventListener("load", () => {
			this.render(path, req.response);
		});
		req.responseType = "json";
		req.open("GET", this.root + "/" + path);
		req.send();
	}

	render(path, files) {
		for (let i = 0; i < files.length; i++) {
			let file = files[i];

			// We don't want to support directories
			if (file.type !== "file") {
				continue;
			}

			let div = document.createElement("div");

			let link = document.createElement("a");
			link.href = this.root + "/" + file.name;
			link.className = "file";

			let name = document.createElement("span");
			name.innerText = file.name;
			name.classList.add("filetype");
			name.classList.add(file.name.split(".").pop());

			let size = document.createElement("span");
			size.innerText = formatSize(file.size);
			size.className = "file-size";

			link.appendChild(name);
			link.appendChild(size);
			div.appendChild(link);
			this.dir.appendChild(div);
		}
	}
}

class FileUpload {
	constructor(rootName) {
		this.root = document.getElementById(rootName);

		let fileInput = this.root.getElementsByTagName("input")[0];
		let chooser = document.getElementById(rootName + "-chooser");
		let progressContainer = document.getElementById(rootName + "-progress");
		let label = progressContainer.getElementsByTagName("label")[0];
		let progress = progressContainer.getElementsByTagName("progress")[0];

		fileInput.addEventListener("change", e => {
			let files = fileInput.files;

			if (files.length === 0) {
				return;
			}

			let bytes = 0;
			for (let i = 0; i < files.length; i++) {
				console.log(files[i]);
				bytes += files[i].size;
			}

			// Hide the upload button and show the progressbar.
			chooser.style.display = "none";
			label.innerText = "Uploading " + files.length + " file" + (files.length > 1 ? "s" : "")
				+ " (" + formatSize(bytes) + ")";
			progressContainer.style.display = "block";
			progress.value = 0;

            const xhr = new XMLHttpRequest();
            const fd = new FormData();

            xhr.open("POST", "/upload_files.php", true);
			xhr.upload.addEventListener("progress", e => {
				if (e.lengthComputable) {
					progress.value = e.loaded / e.total;
				}
			}, false);

			let showUpload = function() {
				chooser.style.display = "block";
				progressContainer.style.display = "none";
			};
            xhr.addEventListener("load", () => {
				showUpload();
            });
			xhr.addEventListener("error", e => {
				alert("Error uploading", e.response);
				showUpload();
			});

			for (let i = 0; i < files.length; i++) {
				fd.append('files[]', files[i]);
			}
            xhr.send(fd);
		});
	}
}

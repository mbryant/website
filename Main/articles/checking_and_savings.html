<meta property="og:description" content="Improving returns on checking/savings accounts by cutting out the middleman"/>
<title>A better banking setup</title>
</head>
<body>
    <div id="main">
        <h1>A better banking setup</h1>
        <h2></h2>
        <p>
        Having money readily available is an important piece of any financial planning.  This allows you to do useful things, like exchange money for goods and services, pay bills, or deal with any emergencies that may arise.  Typically, having readily available money is implemented by putting it in a bank<sup id="target-1"><a href="#footnote-1">1</a></sup> which will allow you to quickly move it elsewhere or withdraw it when needed.  In order to compete for your business, the bank also promises to pay you a percentage on any money you deposit &mdash; it takes your money, does <i>something magical</i> with it, and pays you interest.
        <br><br>

        This sounds great in theory<sup id="target-2"><a href="#footnote-2">2</a></sup>, but for some reason banks don't seem to try particularly hard to compete for your business.  This is likely explained by the high overhead for individuals to move money between banks &mdash; when you're looking for a new bank you're likely to choose the one that'll pay the most interest while having the features you want, but you're much less likely to switch to a different bank that offers a better interest rate six months later.
        <br><br>

        This captive audience leads to weirdness and inefficiencies that most people are probably familiar with.  Have you ever wondered why checking accounts seem so much more useful than savings accounts but always pay suspiciously close to 0.0% percent interest rates?  Have you ever seen in the news that interest rates are rising rapidly, but noticed that your savings account's interest rate hasn't changed in five years?  For that matter, have you ever wondered why banks seem to offer interest rates of barely over 4% when the <a href="https://www.newyorkfed.org/markets/reference-rates/effr" target="_blank">U.S. Federal Funds Interest Rate</a> (the rate they can borrow money at) is well over 5%?
        <br><br>

        Let's cut out the middleman and find a banking setup that provides the same benefits as traditional banking while paying substantially higher interest!
        <br><br>

        <i>Note: this is only my personal opinion on the best banking setup as of August 2024.  I've done my best to describe the benefits and drawbacks of this approach, but you'll need to evaluate the trade-offs for yourself.  If you'd like to skip that, you can <a href="#concrete-proposal">view my concrete proposal</a>.</i>
        </p>

        <h2>Important bank features</h2>
        <p>
        I want a few key things out of a bank<sup id="target-3"><a href="#footnote-3">3</a></sup>.
        </p>
        <h3>Quick access to money</h3>
        <p>
        My bank should be the fastest place for me to get access to my money.  This means I should be able to transfer money to accounts at other institutions within a couple days and do roughly instant wire transfers or ATM withdrawals.
        </p>
        <h3>Low risk</h3>
        <p>
        My money should be safe.  Barring a total collapse of the U.S. government, I want whatever money I put into the bank to still be there when I try to take it out.
        </p>
        <h3>Low overhead (bonus)</h3>
        <p>
        I don't want to pay much for these features.  In particular, I expect ATM withdrawals to be free and I want my interest rate to be as high as possible.  I also want low <i>cognitive</i> overhead, and don't want to think about limits on numbers of transactions per month, minimum balances, overdraft fees, and other annoying things like that.
        </p>

        <h2>Using a broker for cash management</h2>
        <p>
        Ideally we wouldn't even hold cash in a bank account - we'd hold some magical asset returning the <a href="https://en.wikipedia.org/wiki/Risk-free_rate" target="_blank">risk-free rate</a>, and any time we wanted to interact with our money we'd automatically sell it and work with the underlying cash.  If we can pair this magical asset and automation with a bank that provides the features we want, we'll have a great solution.
        <br><br>
        In practice, the magical asset is known as a <a href="https://investor.vanguard.com/investment-products/money-markets" target="_blank">money market fund</a>.  Money market funds invest in a mix of underlying assets &mdash; for example, <a href="https://investor.vanguard.com/investment-products/mutual-funds/profile/vmfxx" target="_blank">VMFXX</a> currently holds 23% U.S. treasury bills, 33.5% U.S. debt, and 43.5% short term low-risk repurchase agreements.  As a result of the low-risk underlying holdings, they're one of the lowest risk asset classes you can find.  Large money market funds also offer enough liquidity for us to treat them as cash &mdash; on any given day hundreds of millions of dollars will flow in and out of VMFXX, meaning you can always convert between cash and VMFXX, and they're insured via <a href="https://www.sipc.org/for-investors/what-sipc-protects" target="_blank">SIPC</a><sup id="target-4"><a href="#footnote-4">4</a></sup>.
        <br><br>
        Any broker will allow us to buy low-risk money market funds, so now all we need to do is find a broker that will let us automatically buy them when we deposit cash and automatically sell them when we try to withdraw cash.  There are actually a few options here, though they'll each take a bit of setup.
        </p>

        <h2 id="concrete-proposal">Concrete proposal</h2>
        <p>
        As of September 2024, <a href="https://www.fidelity.com/" target="_blank">Fidelity</a> seems to be the best option.
        In particular, Fidelity offers a Cash Management Account (CMA) that functions exactly like a standard checking account (checking off our remaining important bank features) and even covers ATM fees (bonus!).  Their CMA account also allows you to choose between holding FDIC-insured cash and <a href="https://institutional.fidelity.com/app/funds-and-products/458/fidelity-government-money-market-fund-spaxx.html" target="_blank">SPAXX</a> (a popular money market fund).

        To use this, take the following steps:
        </p>
        <ol>
            <li>Open a CMA with Fidelity.</li>
            <li>Under <i>Positions</i>, switch your <i>Core Position</i> to be SPAXX.</li>
            <li>Treat it like a standard checking account!</li>
        </ol>

        <p>
        This allows you to have a single account that earns the risk-free rate on all money you have deposited.  As of the time of this writing, switching from Ally Bank's high-yield savings account to this approach would bump your interest rate from 4.2% to 4.96%<sup id="target-5"><a href="#footnote-5">5</a></sup>.  In practice, it's even better than that, since you need both a checking (0.1% interest rate currently) and savings account at Ally due to transaction limits, meaning you'd need to do frequent manual transactions to rebalance between accounts in order to maximize your returns.
        </p>

        <h2>Known downsides</h2>
        <p>
        I'll update this as I encounter more.  So far it's looking pretty good.
        </p>

        <h3>No Zelle</h3>
        <p>
        Fidelity's CMA has no <a href="https://www.zellepay.com/" target="_blank">Zelle</a> support.  This is somewhat annoying, but is mitigated by having a Bill Pay feature that will write and mail checks on your behalf, as well as integrating reasonably with Paypal and Venmo.  In the worst case, Zelle support can be hacked up by finding a bank that supports it and automating transfers from CMA &rarr; Zelle Bank &rarr; Zelle target.
        </p>

        <h3>Not recognized by popular account linking software</h3>
        <p>
        When you need to add a funding source for an external account (credit cards, etc), you're often given the option to immediately link your bank by logging into its account.  Since Fidelity isn't a standard bank, you'll probably need to link manually by waiting a few days for small verification transactions to be sent to your CMA.
        <br><br>
        This isn't an actual issue to me, since I don't trust the current linking systems.  Maybe one day the financial industry will get its act together, but for now I'd prefer to be the only thing logging into my accounts.
        </p>

        <h3>Long holding period after transfers</h3>
        <p>
        After money is transferred into your account, you don't appear to be able to withdraw it for a week.  This isn't normally an issue at a brokerage since you rarely withdraw, but it's more problematic for a place that's holding your liquid money.  Luckily, direct deposited paychecks don't seem to have this limit.
        </p>

        <div id="footnotes">
            <h3>Footnotes</h3>
            <p>
            <span id="footnote-1"><a href="#target-1">[1]</a></span> Whether this is a brick-and-mortar bank, a credit union, or an online-only "high yield" bank isn't particularly important.  For our purposes, a bank is any place that holds your money and pays some fixed interest rate on it.
            <br>
            <span id="footnote-2"><a href="#target-2">[2]</a></span> Banks provide a useful service to consumers, and in exchange they get to hold money that they can then invest behind the scenes.  A competitive and liquid market will lead to them paying out interest rates slightly less than the risk-free rate &mdash; consumers get access to important financial product and banks will race to provide it with the lowest overhead.  In theory at least...</span>
            <br>
            <span id="footnote-3"><a href="#target-3">[3]</a></span> Notably, I'm leaving out a few things you might've thought about, like "a fantastic mobile app".  Since I'd like to have minimal direct interactions with my bank, I'm more interested in the direct financial features than usability, etc.</span>
            <br>
            <span id="footnote-4"><a href="#target-4">[4]</a></span> Which is somewhat different than FDIC insurance, but not in a way that seems meaningful with regards to money market funds.</span>
            <br>
            <span id="footnote-5"><a href="#target-5">[5]</a></span> To visualize why this small of an interest rate difference matters, try <a href="https://www.wolframalpha.com/input?i=graph+1000+e%5E%280.0496*x%29+vs+1000+e%5E%280.042*x%29+for+x+in+%5B0%2C30%5D" target="_blank">graphing</a> the growth of $1,000 over 30 years under the two different interest rates.</span>
            </p>
        </div>

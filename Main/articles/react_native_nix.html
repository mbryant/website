<meta property="og:description" content="Using React Native with Nix to build an Android app"/>
<title>Building an Android app using React Native and Nix</title>
</head>
<body>
	<div id="main">
        <h1>Building an Android app using Nix</h1>
		<h2>How we got here</h2>
		<p>
        <i>Note: this page walks through my process of getting to a decent setup for Android development.  If you're not interested in why I think this approach is decent and just want to see the code, you might want to skip the first few sections.</i><br><br>
        So far in my career I had somehow avoided doing any native mobile development.
        Unfortunately, my sister gave me a good (easy to build, useful, and profitable) app idea, so I decided to try my hand at making an Android app.<br><br>

        I started with the recommended <a href="https://developer.android.com/studio" target="_blank">Android Studio</a> and <a href="https://kotlinlang.org/" target="_blank">Kotlin</a>, but quickly became overwhelmed by the complex UI, stupendous amounts of <a href="https://en.wikipedia.org/wiki/XML" target="_blank">XML</a>, and incomprehensible error messages.  It became clear that I wouldn't be able to write the app I wanted over the afternoon I had reserved, so I decided to step back and reevaluate my approach.
		</p>

		<h2>React Native to the rescue</h2>
		<p>
        I realized that <a href="https://reactnative.dev/" target="_blank">React Native</a> could solve some of my problems.  Rather than needing to learn a new development paradigm, environment, and language in order to succeed with Android Studio/Kotlin, I could write some "simple" Javascript.  While React Native was new to me, I had worked with declarative UI before and have long been a proponent of it.
		<br><br>

        Unfortunately, when I looked at how to get started with React Native, I remembered one of the reasons I've never played with it before:  installing it requires a handful of dependencies, including <a href="https://nodejs.org" target="_blank">Node</a>, Android Studio, and Java.  I'm not a fan of manually managing lots of dependencies, so I immediately thought about using Nix.
		</p>

        <h2>Nix</h2>
        <p>
        <a href="https://nixos.org/" target="_blank">Nix</a> is a tool that allows you to manage packages and configuration in a declarative and reproducible fashion via a functional language.  In practical terms<sup id="target-1"><a href="#footnote-1">1</a></sup>, using Nix means programs you install both have all of their dependencies specified and are somewhat isolated from your main system, allowing you to install packages without fear of breaking things and without needing to manage their dependencies.
        <br><br>
        When I need to install packages, especially if they have multiple dependencies, I immediately look to Nix.  However, this project was slightly different than the other things I'd used Nix for.  I'd historically used Nix to manage programs that I either wanted installed permanently or just wanted to run once or twice.  When building an app, I instead wanted to have a complicated development environment setup and in use for a while, but stored in such a way that it both wouldn't permanently bloat my machine's standard environment and could be shared with other developers who wanted to work on the project<sup id="target-2"><a href="#footnote-2">2</a></sup>.
        <br><br>
        Luckily, there's a tool that lets us manage individual projects with Nix!
        </p>

        <h2>Devenv</h2>
        <p>
        I'd seen an interesting project built on top of Nix called <a href="https://devenv.sh/" target="_blank">Devenv</a> a few weeks prior and thought it might fit.  It was made to solve almost exactly the problem I had &mdash; configuring a per-project development environment in a way that can easily be shared with other collaborators.
        <br><br>
        Unfortunately, installing Devenv in the way I wanted to was a bit more complicated than I expected<sup id="target-3"><a href="#footnote-3">3</a></sup>.  There were a few installation options, but none that looked like a clean fit with my system's configuration, which is managed via <a href="https://github.com/nix-community/home-manager" target="_blank">home-manager</a> using <a href="https://nixos.wiki/wiki/Flakes" target="_blank">flakes</a>.  After a bit of poking around, I came up with the following<sup id="target-4"><a href="#footnote-4">4</a></sup>:
        </p>
		<code>
			<pre>
# Using the standard skeleton for home-manager flakes
inputs = {
  # Include the devenv flake
  devenv.url = "github:cachix/devenv/latest";
  ...
}

# We'll consume devenv in outputs, and insert it into nixpkgs
outputs = {
... home-manager.lib.homeManagerConfiguration {
  pkgs = import nixpkgs {
    system = "x86_64-linux";
    overlays = [
      # Devenv isn't a "real" overlay, but we can fake it
      (self: super: { devenv = devenv.packages.x86_64-linux.devenv; })
    ];
  }
...
}</pre>
		</code>
        <p>
        Now, my <tt>home-manager</tt> configuration could treat <tt>devenv</tt> like any other package and ensure it's properly setup on my machines.
        </p>

		<h2>Putting it together</h2>
		<p>
		Finally, the backstory is complete and we have all the pieces and can put together a reproducible dev environment that'll support React Native for Android!
        <br><br>
        We'll create a new folder for our project, use <tt>devenv init</tt> to setup some default configuration files.  We'll be touching two of these files, starting with <tt>devenv.yaml</tt>:
        </p>

		<code>
			<pre>
inputs:
  nixpkgs: # Default - allow us to install packages from Nix's repository
    url: github:NixOS/nixpkgs/nixpkgs-unstable
  android-nixpkgs:  # Use a repository that makes Android configuration easy
    url: github:tadfisher/android-nixpkgs/stable</pre>
		</code>

        <p>
        Next, we'll define the dependencies that are needed for our project in <tt>devenv.nix</tt>:
        </p>

		<code>
			<pre>
{ inputs, pkgs, ... }:

let
  # Configure which Android tools we'll need (mostly the recommended ones)
  sdk = (import inputs.android-nixpkgs { }).sdk (sdkPkgs:
    with sdkPkgs; [
      build-tools-30-0-3
      build-tools-33-0-0
      cmdline-tools-latest
      emulator
      patcher-v4
      platform-tools
      platforms-android-33
      system-images-android-32-google-apis-x86-64
    ]);
in {
  # Install various packages from Nix that we'll need for this project
  packages = with pkgs; [
    nodePackages.react-native-cli
    watchman
  ];

  # Ensure our path has various Android SDK things in it
  enterShell = ''
    export PATH="${sdk}/bin:$PATH"
    ${(builtins.readFile "${sdk}/nix-support/setup-hook")}
  '';
}</pre>
		</code>

		<p>
        We've now defined our entire development environment in code, and we we can start using it by running <tt>devenv shell</tt>!  Note that we didn't explicitly install some of the dependencies we knew we'd need &mdash;  for example, Nix knows <tt>react-native-cli</tt> relies on Node, so it'll be automatically installed for us.
        <br><br>
        However, there are various commands that we'll need to remember to run while working on the project.  Let's put them into code too by adding to the bottom of <tt>devenv.nix</tt>:
		</p>

		<code>
			<pre>
  # Create the initial AVD that's needed by the emulator
  scripts.create-avd.exec = "avdmanager create avd --force --name phone --package 'system-images;android-32;google_apis;x86_64'";

  # These processes will all run whenever we run `devenv run`
  processes.emulator.exec = "emulator -avd phone -skin 720x1280";
  processes.react-native.exec = "npx react-native start";</pre>
		</code>

        <p>
        This will let us run <tt>create-avd</tt> to create a standard Android Virtual Device (AVD) for our emulator to use.  After that, if we run <tt>devenv up</tt>, the Android emulator and React Native will both start, leaving us to focus entirely on developing our app.
        <br><br>
        We now have an entirely reproducible development environment that's stored in code and backed by Nix.  If other developers join the project or we come back to it after a long period of time, we know it'll continue to work exactly the same as it does today.
        <div id="footnotes">
            <h3>Footnotes</h3>
            <p>
            <span id="footnote-1"><a href="#target-1">[1]</a></span> Nix has many things that aren't practical or easily explained.  It has a super exciting set of features and I'm betting that it continues to improve and grow in popularity, but in the meantime it's hard to describe in a convincing manner.
            <br>
            <span id="footnote-2"><a href="#target-2">[2]</a></span> There are few things worse than checking out a repo and not being able to build or run it without reverse engineering the previous development environment.
            <br>
            <span id="footnote-3"><a href="#target-3">[3]</a></span> This is overly picky, and there's a one-line command that most people would probably use to get <tt>devenv</tt> installed.  That being said, avoiding that one-liner was a good learning experience, and I'm happier with the end result.
            <br>
            <span id="footnote-4"><a href="#target-4">[4]</a></span> I don't expect many readers to understand this and it's not terribly relevant, but I included it in case someone else comes looking for a "cleaner" way to install <tt>devenv</tt> with <tt>home-manager</tt>.
            </p>
        </div>

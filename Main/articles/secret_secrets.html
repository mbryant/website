<meta property="og:description" content="Some issues with standard practices around secrets, along with some alternatives"/>
<title>How secret are your secrets?</title>
</head>
<body>
    <div id="main">
        <h1>How secret are your secrets?</h1>
        <h2></h2>
        <p>
        You have a bunch of machines and a bunch of credentials they (and no one else) need access to, so you use a <a href="https://www.vaultproject.io/" target="_blank">secrets store</a> to store the credentials and distribute them as needed.
        Now only the machines that need a specific secret can access it, so you know the credentials are safe &mdash; or are they?
        <br><br>
        The whole concept of secrets in software is weird.  We run systems composed of millions of lines of code that we've never looked at, expect to be attacked and hope to be resilient against the attackers, and generally can't trust the machines that run our code to be secure.  How can we expect to keep anything important secret in this environment?
        <br><br>
        I'd argue that we can't, and that most secrets stores are little better than distributing text files containing credentials to machines.  As an industry, we've couched our practices around secrets in a set of security jargon (audit log, encryption, identity-based access, etc) that allows us to ignore the glaring holes rather than raise our standards.  I think we can do better, and will discuss some places where current approaches go wrong as well as a few improvements and alternatives we should be using more broadly.
        </p>

        <h2>Secrets in a low-trust environment</h2>
        <p>
        If you have a secure system where you know what code is running and don't allow humans (including attackers) to do arbitrary things, our current approaches to secrets work pretty well.  We've moved to <a href="https://en.wikipedia.org/wiki/Identity-based_security" target="_blank">identity-based access</a>, ensuring machines (or services on them) provide a proof of identity when accessing secrets and allowing us to restrict secrets access to the minimal set of services that need them.  We can keep an audit log of all times secrets are accessed, allowing us to automatically remove access permissions from services that stop needing them,  as well as detecting behavioural anomalies around access patterns.  We even use encryption, both in transit and at rest, to make it harder of attackers to intercept our secrets.  Unfortunately, our original assumption rarely holds.
        <br><br>
        We're typically writing code that runs in a significantly lower-trust environment than hoped for.  We use copious amounts of <a href="/articles/third_party_dependencies.html" target="_blank">dependencies</a>, an operating system that is <a href="https://zerodium.com/program.html" target="_blank">wildly insecure</a><sup id="target-1"><a href="#footnote-1">1</a></sup> (and probably always will be), and are constantly under attack from <a href="https://adversary.crowdstrike.com" target="_blank">well-funded attackers</a>.  Once an attacker can execute code on our machines, they can swiftly pivot to <tt>root</tt>, and all bets around secrecy of our secrets are off.
        <br><br>
        Let's reexamine the three previous things we mentioned our secrets stores can do, but this time under the context of an attacker with root access to a machine.  The attacker can prove their identity to the secrets store &mdash; they can obviously prove to be the machine, but can also pretend to be a specific service if needed.  This allows the attacker to get access to their desired secrets in a way that's likely indistinguishable from the machine's standard behaviour, bypassing the fancy encryption controls.  The audit log is more interesting &mdash; depending on which service the attackers are impersonating, we may be able to detect anomalies around the secret access and reveal their presence.
        <br><br>
        Luckily (for them at least), the attackers probably don't even need to hit our secrets store!  Because they have root access, they can <a href="https://linux.die.net/man/5/proc" target="_blank">peek</a> directly into our service's memory and extract the secret without leaving any trace.  Our services could mitigate this some by dropping their copies of secrets after they're done using them, and <a href="https://lwn.net/Articles/865256/" target="_blank"><tt>memfd_secret()</tt></a> gives us some tools to make this harder, but neither of these approaches are foolproof or always applicable.
        <br><br>
        Overall, it's impossible to ensure things are kept secret in an environment where attackers have root permissions, and most of the industry runs their code in such an environment.  Without <a href="https://sel4.systems/" target="_blank">revamping all of our underlying systems</a>, what can we do about this?
        </p>

        <h2 id="smaller-scopes">Smaller scopes</h2>
        <p>
        A general philosophy in security is to reduce the scope of systems.  If a machine is hacked, it's much worse for it to have access to all databases than to just one of them.  If a customer support agent is compromised, the fallout will be much smaller if they can only operate against a few customers or types of data rather than everything.  However, this principle often goes out the window with secrets.  We'll talk about using principle of least privilege to give access to a Github secret to only the services that need it, then turn around and create said Github secret with full admin credentials.  This is made even worse by some vendors with insane permission levels &mdash; because you want the ability to perform one innocuous action, the vendor requires you to have permission to <i>all</i> actions.
        <br><br>
        That being said, probably the easiest way to reduce the impact of compromised secrets is to reduce what the secrets can do.  When possible, you should create secrets with the minimal set of permissions they need.  It's a lot cheaper to create a handful of similar secrets with varying permissions than it is to recover from someone abusing an over-permissioned one.  When this isn't possible, we should push the problematic vendors (<i>cough</i> Okta, Hashicorp, Github, etc) to improve the granularity of their access control models, at least for programmatic actors.
        </p>

        <h2>Rotate early, rotate often</h2>
        <p>
        A common scenario is for a secret to contain an API key to some other service.  An attacker that gains access to said secret will want to exfiltrate the secret to a machine they own, then abuse your access in some fashion.  Using the extracted secret from their own machine makes it easier for them to play around and explore with it, and makes it less likely that they'll be discovered inside your infrastructure.
        <br><br>
        An easy way to mitigate this is to have any credentials stored in secrets expire and be rotated frequently.  If a secret expires after 15 minutes, that practically forces an attacker to stay present and visible on your infrastructure in order to use your credentials.  AWS Secrets Manager exposes a <a href="https://docs.aws.amazon.com/secretsmanager/latest/userguide/rotating-secrets.html" target="_blank">pretty convenient system</a> for doing this, and whichever secrets store you're using probably does too.  Unfortunately, while it's feasible to implement this right now, it can be pretty annoying, and feels like too much work to foist onto arbitrary developers that just want to store credentials securely.
        <br><br>
        I'd like to see the industry progress here &mdash; vendors should make it easy to rotate credentials (and hard to not rotate them!).  All vendors should expose an API that takes some seed credential (acquired once by a human), which is then used to generate actual credentials.  If this was standardized enough, secrets stores could store only the seed credential, then automatically fetch temporary credentials whenever secrets are requested by a client.  In the meantime, consider implementing something like this for credentials to services that are either important or prolific with their credentials.
        </p>

        <h2>Building high-trust environments</h2>
        <p>
        We've already talked about how we can't trust most of our machines, but maybe we can trust some of them.  If we had machines capable of running small amounts of code that we trust, we could safely use our secrets store from those machines.  Luckily, we have <a href="https://aws.amazon.com/lambda/" target=_blank">various</a> <a href="https://nanovms.com/" target=_blank">options</a> <a href="https://sel4.systems/" target=_blank">here</a>!  This doesn't directly solve our problems &mdash; we don't want small amounts of code we trust to access secrets, we want our services to!  However, it gives us a base we can build a more secure approach on top of.
        <br><br>
        In the common scenario for secrets, we're storing API keys for some service.  Machines fetch a secret containing credentials for some service, then build an HTTP request that uses the credentials as some form of authentication &mdash; probably via the dubiously named <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Authorization" target="_blank"><tt>Authorization header</tt></a>.  This might look something like the following:
        </p>
        <code><pre>
$ TOKEN=$(aws secretsmanager get-secret-value --secret-id fakevendor-admin-creds | jq .SecretString)
$ curl -H "Authorization: Bearer ${TOKEN}" https://api.fakevendor.com/whoami
{'user': 'admin'}</pre>
        </code>

        <p>
        Rather than having the machine make this request directly using the credentials, let's create a proxy service that's able to inject credentials into requests<sup id="target-2"><a href="#footnote-2">2</a></sup>.  This allows us to only give the proxy service access to the credentials, rather than the machine that previously had access.  From our machine, a request under this new approach is pretty simple, since the proxy handles all of the interaction with secrets:
        </p>
        <code><pre>
$ curl -H "Authorization: INTENTIONALLY_LEFT_BLANK" https://fakevendor.secretsproxy.internal/whoami
# The proxy intercepts this request, validates the sending machine,
# overwrites the Authorization header, and forwards it to the original API.
{'user': 'admin'}</pre>
        </code>

        <p>
        To the machine making the request, everything functions the same.  However, attackers on our main machines can no longer extract secrets, since they're never able to see them in the first place.  We've moved secrets access from a machine we don't really trust that's running huge amounts of code with a small proxy service which can be implemented in a few hundred lines of code.  While the proxy service may have vulnerabilities, there's both a lot less code to exploit and a much smaller surface areas to do it with, making it far harder for malicious actors to steal secrets.
        </p>

        <p>
        This approach has a few other notable benefits.
        <br><br>
        <b>Auditability</b>:  Now that secrets can't be silently extracted via a malicious root user or side channels, we can trust our audit log.  Additionally, we can associate some information about the request that was made, giving us the ability to not just see that a secret was used but also how it was used.
        <br><br>
        <b>Smaller scopes</b>:  This can pair well with the <a href="#smaller-scopes"><i>Smaller Scopes</i></a> proposal above.  If you have credentials that are too powerful for the operations that are needed, the secrets proxy can be expanded to do validation on requests.  For example, you could turn a read+write admin credential into a read-only admin credential by only allowing <tt>GET</tt> requests, or restrict it to certain operations by <a href="https://en.wikipedia.org/wiki/Whitelist" target="_blank">whitelisting</a> specific API paths.
        <br><br>
        <b>Network isolation</b>:  If you use some form of network isolation, where your machines can only talk to a whitelisted set of services, the secrets proxy can provide better isolation guarantees.  For example, if <tt>github.com</tt> was whitelisted to allow machines to work with your codebase, nothing stops an attacker from using Github with their own credentials to exfiltrate important data from your environment by committing it to their repository.  With the secrets proxy in place, your machines can only access Github with your credentials, rather than allowing the attacker to bring their own.
        </p>

        <h2>Combining the above</h2>
        <p>
        If we could combine the above approaches, I think the industry would be in a much better position around secrets.  Rather than closing our eyes and pretending everything is secret, we'd be able to confidently say that for many of our secrets, only the machines that need access can ever use them.  For secrets where a proxying approach is infeasible, we'd at least be able to say that attackers need to be active on our machines to use them due to their short lifetimes.
        <br><br>
        None of these ideas are novel or even particularly difficult to implement. However, making these approaches scale across all credentials will require vendors to support some new features, like standardized credential rotation and smaller scopes.  If you're in a position where you're implementing an API with credentials, please keep these ideas in mind!
        </p>

        <div id="footnotes">
            <h3>Footnotes</h3>
            <p>
            <span id="footnote-1"><a href="#target-1">[1]</a></span> You know something has gone wrong when it costs more to buy a car than a local privilege escalation exploit that'll work against billions of machines.
            <br>
            <span id="footnote-2"><a href="#target-2">[2]</a></span> I don't see any published prior art for a secrets proxy like this, so credit for showing its viability goes to Eli Skeggs, who got the original idea from Trey Tacon.</span>
            </p>
        </div>

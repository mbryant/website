let photoList = [];

// Images are dynamically resized, so we only fetch them if there's a
// reasonable expectation that someone will view them.
const observer = new IntersectionObserver(entries => {
    entries.filter(entry => entry.isIntersecting).forEach(entry => {
        const img = entry.target;

        img.src = img.pending_src;
        observer.unobserve(img);
    });
}, {rootMargin: "300px"});

function renderIndex() {
    const folderTemplate = document.getElementById("thumbnail-template");
    const foldersNode = document.getElementById("thumbnails");

    // If we have cookies set, we shouldn't cache the previous list of photos,
    // since we may have authenticated to albums that the cached entry thinks
    // are still private.
    const cache_behavior = document.cookie ? {cache: "reload"} : {};

    fetch('/photo_list.json', cache_behavior)
        .then(response => response.json())
        .then(data => {
            for (const [folder, info] of Object.entries(data)) {
                let titleMut = folder.split(" ");
                const year = titleMut.pop();
                const collection = titleMut.join(" ");

                if (!year.startsWith("2")) {
                    // We ignore photos that aren't tagged by
                    // year.
                    continue;
                }

                photoList.push([year, collection, info]);
            }

            // Show the most recent year first.
            photoList.sort().reverse();

            for (const [year, collection, info] of photoList) {
                const album = `${collection} ${year}`;
                const folderView = folderTemplate.content.cloneNode(true);
                const img = folderView.querySelector("img");

                folderView.querySelector(".thumbnail-name").innerText = collection;
                folderView.querySelector(".thumbnail-year").innerText = year;

                if (info["access"] === "Accessible") {
                    const cover = info.cover_photo;
                    img.pending_src = `thumbnails/${album}/${cover}`;

                    folderView.querySelector(".thumbnail").addEventListener('click', () => {
                        window.location = `album.html?album=${album}`;
                    });
                } else {
                    console.assert(info["access"] === "Private", `Invalid access level for ${album}`);

                    folderView.querySelector(".thumbnail-info").classList.replace("thumbnail-info", "thumbnail-private");
                    img.pending_src = "assets/private.png";

                    folderView.querySelector("form").action = `/authenticate/${album}`;
                }

                observer.observe(img);
                foldersNode.appendChild(folderView);
            }

            // HACK: The hexagon CSS code is gross and can't effectively size
            // the vertical height of our page.  It's easier to fix it in JS.
            const fixHexagons = () => {
                const hexagons = document.getElementById("hexagon-container");
                const images = foldersNode.querySelectorAll("img");
                const lastImage = images[images.length - 1];

                hexagons.style.height = `${lastImage.offsetTop}px`;
            };

            window.addEventListener("resize", fixHexagons);
            fixHexagons();
        });
}

function renderAlbum() {
    const url = new URL(window.location);
    const album = url.searchParams.get("album");

    const photoTemplate = document.getElementById("thumbnail-template");
    const photosNode = document.getElementById("thumbnails");

    fetch(`/directory/${album}/info.json`)
        .then(response => response.json())
        .then(data => {
            // Users should only be rendering albums they can access.
            console.assert(data["access"] === "Accessible", `Attempted to view inaccessible ${album}`);
            photoList = data.photos;

            // Fill in the album count
            document.getElementById("total").innerText = photoList.length;

            if (window.location.hash != "") {
                // We loaded the page and found that we've already requested an
                // image.
                renderPhoto(album, window.location.hash.slice(1) - 1);
            }

            photoList.forEach((photo, i) => {
                const photoView = photoTemplate.content.cloneNode(true);
                const image = photoView.querySelector("img");

                image.pending_src = `thumbnails/${album}/${photo}`;
                observer.observe(image);

                photoView.querySelector(".thumbnail").addEventListener('click', () => renderPhoto(album, i));

                photosNode.appendChild(photoView);
            });

            photosNode.querySelectorAll("img").forEach(img => {
                observer.observe(img);
            });
        });

    // Update the album name in the breadcrumbs
    document.getElementById("album-name").innerText = album;
    document.title = `${album} - photos.programsareproofs.com`

    // Make the close button work properly
    document.getElementById("close").addEventListener("click", () => {
        if (document.fullscreenElement !== null) {
            // We're still fullscreened, which only makes sense when displaying
            // a preview.
            document.exitFullscreen();
        }

        // Stop showing the preview window
        document.getElementById("preview").style.display = "none";

        // Reenable scrolling and return to the previous position
        document.body.style.overflowY = "auto";

        // Remove the hash so we don't return to the last photo on a reload.
        history.replaceState(null, "", window.location.href.split("#")[0]);
    });
}

function renderPhoto(album, photoIndex) {
    const photo = photoList[photoIndex];
    const photoName = document.getElementById("photo-name");
    const previewWindow = document.getElementById("preview");

    // Render the thumbnail to start with, then progressively enhance with new
    // image.
    previewWindow.querySelector("img").src = `thumbnails/${album}/${photo}`;
    previewWindow.querySelector("img").src = `highres/${album}/${photo}`;

    // Disable scrolling on the page
    document.body.style.overflowY = "hidden";

    // Make the left and right buttons work
    document.getElementById("prev").onclick = () => {
        // `%` is actually remainder in Javascript
        const prevPhoto = ((photoIndex - 1) % photoList.length + photoList.length)
            % photoList.length;

        renderPhoto(album, prevPhoto);
    };
    document.getElementById("next").onclick = () => {
        renderPhoto(album, (photoIndex + 1) % photoList.length);
    };

    let download = document.getElementById("download").querySelector("a");
    download.href = `highres/${album}/${photo}`;
    download.download = `${album}_${photo}`.replace(" ", "_");

    let fullscreen = document.getElementById("fullscreen");
    fullscreen.onclick = () => {
        if (document.fullscreenElement === null) {
            // We're currently not fullscreen.  Show the thumbnail in
            // fullscreen.
            previewWindow.requestFullscreen();
        } else {
            // We're fullscreened, so we should exit that.
            document.exitFullscreen();
        }
    };

    document.getElementById("index").innerText = photoIndex + 1;
    history.replaceState(null, "", `#${photoIndex + 1}`);

    // Show the preview window
    previewWindow.style.display = "block";
}

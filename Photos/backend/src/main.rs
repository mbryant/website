use anyhow::Context;
use axum::body;
use axum::extract::{Form, Path, State};
use axum::response::{IntoResponse, Json, Response};
use axum::{
    routing::{get, post},
    Router,
};
use axum_extra::extract::cookie::{Cookie, Key, PrivateCookieJar};
use constant_time_eq::constant_time_eq;
use futures::StreamExt;
use retainer::cache::Cache;
use retainer::entry::CacheExpiration;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::io::Cursor;
use std::path::PathBuf;
use std::sync::Arc;
use tokio::sync::mpsc;
use tokio_stream::wrappers::ReadDirStream;

///////////////////////////////////////////////////////////////////////////////
// State
///////////////////////////////////////////////////////////////////////////////
#[derive(Clone)]
struct PhotosState {
    /// The root directory to serve photos from
    photo_dir: PathBuf,

    /// A temporary directory used to cache resized photos
    cache_dir: PathBuf,

    /// A cache for information about albums to avoid reading from disk.
    ///
    /// This is particularly useful for supporting private albums, since all photo operations need
    /// to check whether they're in an accessible private album.  Caching that piece of information
    /// saves disk reads on every image access.
    directory_info: Arc<Cache<DirectoryName, AlbumInfo>>,

    /// A key used to sign our cookies, allowing users to view private albums.
    cookie_key: Key,

    /// Send a message to shutdown the entire server.
    shutdown_command: mpsc::Sender<()>,
}

impl PhotosState {
    /// Create a new [`PhotosState`] based on environment variables.
    /// Will only be successful if `PHOTO_DIR` is set to an absolute path.
    ///
    /// NOTE: There's no guarantee that all of the directories in the returned struct exist.
    async fn from_env(shutdown_command: mpsc::Sender<()>) -> anyhow::Result<Self> {
        let photo_dir = PathBuf::from(std::env::var("PHOTO_DIR").context("PHOTO_DIR not set")?);
        anyhow::ensure!(
            photo_dir.is_absolute(),
            "PHOTO_DIR must be an absolute path"
        );

        // Ensure we have a cache directory
        let cache_dir = PathBuf::from("/tmp/photos.programsareproofs.com/");
        let _ = tokio::fs::create_dir(&cache_dir).await;

        let key = match tokio::fs::read(&cache_dir.join("key.bytes")).await {
            Ok(bytes) => Key::from(&bytes),
            _ => {
                let key = Key::generate();

                tokio::fs::write(&cache_dir.join("key.bytes"), key.master())
                    .await
                    .context("Failed to create key file")?;

                key
            }
        };

        Ok(Self {
            photo_dir,
            cache_dir,
            shutdown_command,
            directory_info: Arc::new(Cache::new()),
            // Generate a new key every time the server is started, since that's easier than saving
            cookie_key: key,
        })
    }
}

impl axum::extract::FromRef<PhotosState> for Key {
    fn from_ref(state: &PhotosState) -> Self {
        state.cookie_key.clone()
    }
}

///////////////////////////////////////////////////////////////////////////////
// Client-facing data structures
///////////////////////////////////////////////////////////////////////////////
/// Information about albums, separated by whether they're accessible by the requester.
#[derive(Serialize)]
#[serde(tag = "access")]
enum AlbumAccess {
    /// This album isn't accessible without the proper credentials
    Private,

    /// Contains information about the album, which is accessible to the current user
    Accessible {
        /// The cover photo to display for this album
        cover_photo: FileName,

        /// A list of all filenames for photos in this album
        photos: Vec<FileName>,
    },
}

/// An authentication request for some album
#[derive(Deserialize)]
struct AlbumAuth {
    password: String,
}

/// A representation of an album's folder name
type DirectoryName = String;

/// A representation of an image's file name
type FileName = String;

///////////////////////////////////////////////////////////////////////////////
// Server-side data structures
///////////////////////////////////////////////////////////////////////////////

#[derive(Deserialize, Default, Clone)]
struct DirectoryConfig {
    /// If a password is present, the client must provide it in order to access anything about this
    /// directory (other than its name).
    password: Option<String>,

    /// The filename to use as album art for this album
    cover_photo: Option<FileName>,
}

#[derive(Clone)]
struct AlbumInfo {
    config: DirectoryConfig,
    photos: Vec<FileName>,
}

///////////////////////////////////////////////////////////////////////////////
// Routing and configuration
///////////////////////////////////////////////////////////////////////////////
#[tokio::main]
async fn main() -> anyhow::Result<()> {
    pretty_env_logger::formatted_timed_builder()
        .filter_level(log::LevelFilter::Info)
        .init();


    let (shutdown_command, mut shutdown_listener) = mpsc::channel::<()>(1);
    let state = PhotosState::from_env(shutdown_command).await?;
    let socket = "[::]:6400".parse()?;

    log::info!(
        "Serving photos from {} on {}",
        state.photo_dir.display(),
        socket
    );

    let app = Router::new()
        // Disallow all robots from hitting the site.
        // This isn't targeted at the general internet, so crawling it would mostly be a waste of my CPU
        // and bandwidth.
        .route(
            "/robots.txt",
            get(|| async { "User-agent: *\nDisallow: /" }),
        )
        .route("/photo_list.json", get(photo_list))
        .route("/directory/:folder/info.json", get(directory_list))
        .route("/thumbnails/:folder/:photo", get(resize_image::<640>))
        .route("/highres/:folder/:photo", get(resize_image::<2560>))
        .route("/authenticate/:folder", post(authenticate))
        .with_state(state);

    axum::Server::bind(&socket)
        .serve(app.into_make_service())
        .with_graceful_shutdown(async {
            shutdown_listener.recv().await;
            log::error!("Shutdown requested");
        })
        .await
        .context("Failed to launch app")
}

///////////////////////////////////////////////////////////////////////////////
// Authorization
///////////////////////////////////////////////////////////////////////////////
impl AlbumInfo {
    /// Return the information a client needs in order to use the info about this directory.
    fn client_info(
        self,
        folder: &DirectoryName,
        cookies: &PrivateCookieJar,
    ) -> anyhow::Result<AlbumAccess> {
        if self.client_accessible(folder, cookies) {
            (!self.photos.is_empty())
                .then(|| {
                    Ok(AlbumAccess::Accessible {
                        cover_photo: self.config.cover_photo.unwrap_or_else(|| {
                            // There's no cover photo specified, so we pick one ourselves.
                            // This will give each client persistent cover photos for the duration
                            // they cache these files for.
                            use rand::Rng;
                            self.photos[rand::thread_rng().gen_range(0..self.photos.len())].clone()
                        }),
                        photos: self.photos,
                    })
                })
                .context("No photos in this album")?
        } else {
            Ok(AlbumAccess::Private)
        }
    }

    fn client_accessible(&self, folder: &DirectoryName, cookies: &PrivateCookieJar) -> bool {
        // Default to accessible unless there's a config file saying it isn't.
        if let Some(expected) = &self.config.password {
            cookies
                .get(folder)
                .map(|cookie| constant_time_eq(cookie.value().as_bytes(), expected.as_bytes()))
                .unwrap_or(false)
        } else {
            true
        }
    }
}

async fn authenticate(
    State(state): State<PhotosState>,
    Path(album): Path<DirectoryName>,
    cookies: PrivateCookieJar,
    Form(form): Form<AlbumAuth>,
) -> Result<impl IntoResponse, PhotosError> {
    use axum::response::Redirect;

    let info = directory_info(&state, &album).await?;
    let expected = info
        .config
        .password
        .context("Given album doesn't require a password")?;

    Ok(
        if constant_time_eq(form.password.as_bytes(), expected.as_bytes()) {
            log::info!("Successful authentication for `{}`", album);

            // Send them to the album they authenticated to
            let redirect = Redirect::to(&format!("/album.html?album={}", album));

            (
                cookies.add(
                    Cookie::build(album, expected)
                        .secure(true)
                        .path("/")
                        .finish(),
                ),
                redirect,
            )
        } else {
            log::warn!("Failed authentication attempt for `{}`", album);

            (cookies, Redirect::to("/"))
        },
    )
}

///////////////////////////////////////////////////////////////////////////////
// Album information
///////////////////////////////////////////////////////////////////////////////
async fn directory_info(state: &PhotosState, dir: &DirectoryName) -> anyhow::Result<AlbumInfo> {
    if let Some(cached) = state.directory_info.get(dir).await {
        log::debug!("Found cached directory info for `{}`", dir);
        Ok((*cached).clone())
    } else {
        log::info!("Reading directory info for `{}`", dir);
        let base = state.photo_dir.join(dir);

        if base
            .components()
            .any(|x| x == std::path::Component::ParentDir)
        {
            return Err(anyhow::anyhow!("Directory traversal detected"));
        }

        // Read the config file for the directory
        let config: DirectoryConfig = match tokio::fs::read(&base.join("config.json")).await {
            Ok(bytes) => serde_json::from_slice(&bytes)?,
            _ => Default::default(),
        };

        let mut photos = ReadDirStream::new(
            tokio::fs::read_dir(&base)
                .await
                .context("No folder for the given album")?,
        )
        .filter_map(|entry| async move {
            entry.ok()?.file_name().to_str().and_then(|name| {
                // We only support JPEGs
                name.to_lowercase()
                    .ends_with(".jpg")
                    .then(|| name.to_string())
            })
        })
        .collect::<Vec<_>>()
        .await;
        photos.sort();

        let cache_entry = AlbumInfo { config, photos };

        let _ = state
            .directory_info
            .insert(
                dir.to_string(),
                cache_entry.clone(),
                // Chosen arbitrarily, but should be fast enough to reflect manual file changes
                // without being annoying while also slow enough to not cause much disk traffic.
                CacheExpiration::from(std::time::Duration::from_secs(30)),
            )
            .await;

        Ok(cache_entry)
    }
}

async fn photo_list(
    State(state): State<PhotosState>,
    cookies: PrivateCookieJar,
) -> Result<impl IntoResponse, PhotosError> {
    let folders = ReadDirStream::new(
        tokio::fs::read_dir(&state.photo_dir)
            .await
            .context("No such PHOTO_DIR")?,
    )
    .filter_map(|entry| {
        let state = &state;
        let cookies = &cookies;

        async move {
            let entry = entry.ok()?;
            let path = entry.path();

            let metadata = tokio::fs::metadata(&path).await.ok()?;

            if metadata.is_dir() {
                let folder_name = entry.file_name().into_string().ok()?;

                let info = directory_info(state, &folder_name).await.ok()?;

                info.client_info(&folder_name, cookies)
                    .ok()
                    .map(|info| (folder_name, info))
            } else {
                None
            }
        }
    })
    .collect()
    .await;

    Ok((
        [("Cache-Control", "private, max-age=300")],
        Json::<HashMap<DirectoryName, AlbumAccess>>(folders),
    ))
}

async fn directory_list(
    State(state): State<PhotosState>,
    Path(dir): Path<DirectoryName>,
    cookies: PrivateCookieJar,
) -> Result<impl IntoResponse, PhotosError> {
    let info = directory_info(&state, &dir).await?;

    Ok((
        // We can return an immutable response here because a well-behaved client should never
        // request to view a private album.
        [("Cache-Control", "private, max-age=300, immutable")],
        Json(info.client_info(&dir, &cookies)?),
    ))
}

///////////////////////////////////////////////////////////////////////////////
// Image queries
///////////////////////////////////////////////////////////////////////////////
/// Small module for validating JPEG paths that the client is asking us to load.
mod jpegpath {
    use crate::*;

    #[derive(serde::Deserialize, Debug)]
    pub(crate) struct Client {
        folder: String,
        photo: String,
    }

    pub struct Validated {
        pub folder: String,
        pub photo: String,
    }

    impl Client {
        /// Validate that the specified folder directory both exists, is accessible to us, and isn't
        /// trying to trick us into a directory traversal attack.
        pub(crate) async fn validate(
            self,
            state: &PhotosState,
            cookies: &PrivateCookieJar,
        ) -> anyhow::Result<Validated> {
            anyhow::ensure!(
                self.photo.to_lowercase().ends_with(".jpg"),
                "Can only load JPEGs"
            );

            // This validates that the folder exists
            let info = directory_info(state, &self.folder).await?;

            if info.client_accessible(&self.folder, cookies) {
                Ok(Validated {
                    folder: self.folder,
                    photo: self.photo,
                })
            } else {
                Err(anyhow::anyhow!("Private image"))
            }
        }
    }
}

async fn resize_image<const DIM: u32>(
    State(state): State<PhotosState>,
    Path(target): Path<jpegpath::Client>,
    cookies: PrivateCookieJar,
) -> Result<Response<body::Full<body::Bytes>>, PhotosError> {
    let target = target.validate(&state, &cookies).await?;

    let path = {
        // Find the full path of the image, canonicalizing it as needed.
        let image: PathBuf = [
            state
                .photo_dir
                .to_str()
                .context("PHOTO_DIR is no longer valid unicode")?,
            &target.folder,
            &target.photo,
        ]
        .iter()
        .collect();

        // Validate that we're not being hit with a directory traversal attack.
        // 1. We know everything but the photo filename is reasonable due to `validate`.
        // 2. The filename for the image we're loading shouldn't change under canonicalization
        let _ = image
            .file_name()
            .and_then(|name| name.to_str().map(|x| x == target.photo))
            .context("Suspected directory traversal attack")?;

        image
    };

    // `target.folder` and `target.photo` have both been validated already
    let download_name = format!(
        "{}_{}_{}",
        DIM,
        target.folder.replace(' ', "_"),
        target.photo
    );

    // Hit our cache first to hopefully save the expensive resize operation
    let cached_img = state.cache_dir.join(&download_name);

    let bytes = match tokio::fs::read(&cached_img).await {
        Ok(bytes) => {
            log::debug!(
                "Using cached {dim}x{dim} image for {}",
                path.display(),
                dim = DIM
            );

            bytes
        }
        _ => {
            log::info!(
                "Rendering {dim}x{dim} image for {}",
                path.display(),
                dim = DIM
            );

            let bytes = tokio::fs::read(path).await.context("Failed to read image")?;

            // Read the entire JPEG, then resize to fit.
            let image = {
                let original =
                    image::io::Reader::with_format(Cursor::new(&bytes), image::ImageFormat::Jpeg)
                        .decode()?;

                if DIM < 1024 {
                    // We're generating a small image, so let's take the optimized thumbnail pathway.
                    original.thumbnail(DIM, DIM)
                } else {
                    original.resize(DIM, DIM, image::imageops::FilterType::Lanczos3)
                }
            };

            // Read EXIF to get the orientation, then rotate the image accordingly
            let orientation = {
                let exifreader = exif::Reader::new();

                exifreader
                    .read_from_container(&mut Cursor::new(&bytes))?
                    .get_field(exif::Tag::Orientation, exif::In::PRIMARY)
                    .and_then(|orientation| orientation.value.get_uint(0))
            };

            let output = match orientation {
                Some(8) => image.rotate270(),
                Some(3) => image.rotate180(),
                Some(6) => image.rotate90(),
                _ => image,
            };

            // Reencode our resized image, which has the side effect of dropping EXIF data.
            let bytes = {
                let mut encoded = Vec::with_capacity((DIM / 10 * 1024) as usize);
                let mut encoder = image::codecs::jpeg::JpegEncoder::new_with_quality(&mut encoded, 100);
                encoder.encode_image(&output)?;

                encoded
            };

            // Write the converted image to our cache
            if tokio::fs::write(cached_img, &bytes).await.is_err() {
                // This can fail if our cache has been deleted.  Since we're using a temp
                // directory, this can happen after a few days.  Rather than trying to update all
                // of our state, shutdown and let reinitialization handle this properly.
                if state.shutdown_command.try_send(()).is_ok() {
                    // Only one sender will succeed at requesting a shutdown, so we don't block
                    // on the request.
                    log::error!("Cache directory was deleted");
                }
            }
            bytes
        }
    };

    Ok(Response::builder()
        .header("Content-Type", "image/jpeg")
        .header("Cache-Control", "private, max-age=2592000, immutable") // 1m client-side cache
        .header(
            "Content-Disposition",
            format!("attachment; filename=\"{}\"", download_name),
        )
        .body(body::Full::from(bytes))?)
}

///////////////////////////////////////////////////////////////////////////////
// Anyhow wrapper
///////////////////////////////////////////////////////////////////////////////
/// To make Anyhow work reasonably with Axum, we need to provide some way for Axum to respond when
/// we give it an [`anyhow::Error`].
struct PhotosError(anyhow::Error);

impl IntoResponse for PhotosError {
    fn into_response(self) -> Response {
        (
            axum::http::StatusCode::INTERNAL_SERVER_ERROR,
            format!("{:?}", self.0),
        )
            .into_response()
    }
}

impl<E> From<E> for PhotosError
where
    E: Into<anyhow::Error>,
{
    fn from(err: E) -> Self {
        Self(err.into())
    }
}

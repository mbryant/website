let SidebarList = class {
    constructor(listBase, onfocus) {
        this.list     = listBase;
        this.slideout = document.createElement("div");
        this.access   = onfocus;
        this.data     = [];
        this.focused  = null;

        // Create the list title
        let title = document.createElement("div");
        title.classList.add("list-item", "list-title");
        title.innerText = this.list.innerText;
        this.list.innerHTML = "";
        this.list.appendChild(title);

        // Create the list slideout
        this.slideout.innerHTML = "&#9776;";
        this.slideout.classList.add("list-slideout");
        this.slideout.addEventListener("click", () => {
            // Replace the hamburger menu with the full list
            this.slideout.classList.remove("list-slideout-active");
            this.list.classList.add("list-active");
        });
        this.list.insertAdjacentElement("afterend", this.slideout);

        // Make the list active and don't show the hamburger by default
        this.list.classList.add("list-active");

        // TODO: Create search element and handle searching
    }

    addElement(data) {
        let id   = this.data.push(data) - 1;
        let elem = document.createElement("div");

        data.elem = elem;
        elem.classList.add("list-item");
        elem.innerText = data.text;
        elem.onclick = () => { this.onclick(id); };

        // Return the new id, and expose the list element it corresponds to.
        // TODO: exposing this doesn't feel good - can we refactor?
        data.contents.listElement = elem;
        return id;
    }

    onclick(id) {
        // Show the hamburger menu and hide the full list
        this.list.classList.remove("list-active");
        this.slideout.classList.add("list-slideout-active");

        if (id === this.focused) {
            // We focused on the element currently being viewed.
            return;
        }

        if (this.focused !== null) {
            // There was a previously focused element that we should defocus.
            let oldFocus = this.data[this.focused];

            oldFocus.elem.classList.remove("list-item-selected");
            oldFocus.elem.classList.add("list-item-viewed");
        }

        let newFocus = this.data[id];
        this.focused = id;

        // Focus the new element and scroll it into the center of the list.
        newFocus.elem.classList.remove("list-item-viewed");
        newFocus.elem.classList.add("list-item-selected");
        newFocus.elem.scrollIntoView({
            "behaviour": "smooth",
            "block"    : "center",
            "inline"   : "center",
        });

        // Update the page hash to keep elements linkable
        location.hash = "#" + encodeURIComponent(newFocus.text.replace(/ /g, "_"));

        this.access(newFocus.contents);
    }

    hashEvent() {
        // The url hash may be an encoded version of a list element.  If so,
        // focus the targetted element.
        const elementName = decodeURIComponent(location.hash.substr(1).replace(/_/g, " "));

        this.data.some((element, i) => {
            if (element.text === elementName) {
                this.onclick(i);
                return true;
            }
        });
    }

    render() {
        let fragment = document.createDocumentFragment();

        this.data.forEach(d => {
            fragment.appendChild(d.elem);
        });

        this.list.appendChild(fragment);

        // Check if there was a hash specified on load.  This means we should
        // be loading directly to a list element.
        this.hashEvent();

        // Listen for hash updates.  These occur on forward/back navigation,
        // and notify us to focus on a new element.
        window.addEventListener("hashchange", () => {
            // The hash was updated, so we probably want to render a new element.
            this.hashEvent();
        });
    }
}

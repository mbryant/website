<?php
	if (!isset($_POST['json'])) {
		exit();
	}

	$content = $_POST['json'];

	// Maximum length of shopping list
	if (strlen($content) > 128000) {
		exit();
	}
	if (strpos($content, "<") !== FALSE || strpos($content, ">") !== FALSE) {
		exit();
	}

	// Check for valid json before writing the file
	json_decode($content, true);
	if (json_last_error() != JSON_ERROR_NONE) {
		exit();
	}

	file_put_contents("list.json", strtolower($content));
?>


// Random Number class
var Random = new function () {
	// Return an int within [min, max]
	this.nextInt = function(min, max) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	};
	// Return a float within [min, max]
	this.nextFloat = function(min, max) {
		return Math.random() * (max - min + 1) + min;
	};
	// Return a bool with p probability of being true
	this.nextBool = function(p) {
		return Math.random() < p;
	};
};

// Hold the ball data
var Balls;

// Calculate the new ball positions
var calcBall = function(ball) {
	// Randomly angle ball
	if (Random.nextBool(.1)) {
		ball.yVel += Random.nextInt(-1, 1);
		ball.xVel += Random.nextInt(-1, 1);
		if (ball.xVel > 3 || ball.xVel < -3)
			ball.xVel /= 2;
		if (ball.yVel > 3 || ball.yVel < -3)
			ball.yVel /= 2;
	}

	// Check bounding box of window
	if (ball.y + ball.radius > Balls.canvasHeight && ball.yVel >= 0)
		ball.yVel *= -1;
	else if (ball.y - ball.radius < 0 && ball.yVel <= 0)
		ball.yVel *= -1;
	if (ball.x + ball.radius > Balls.canvasWidth*2 && ball.xVel >= 0)
		ball.xVel *= -1;
	else if (ball.x - ball.radius < 0 && ball.xVel <= 0)
		ball.xVel *= -1;
	ball.x += ball.xVel;
	ball.y += ball.yVel;
};

// Listen for message to start
self.addEventListener('message', function(event) {
	// Parse the sent JSON
	var parseData = JSON.parse(event.data.substring(4));

	// Handle the instructions
	switch (event.data.substring(0,4)) {
		case "init":
			moveBalls();
			break;
		case "data":
			Balls = parseData;
			break;
		case "size":
			Balls.canvasWidth = parseData[0];
			Balls.canvasHeight = parseData[1];
			break;
		case "drop":
			Balls.numBalls--;
			Balls.ball.splice(parseData, 1);
			break;
	}
}, false);

function moveBalls() {
	for (var i = Balls.numBalls - 1; i >= 0; i--)
		calcBall(Balls.ball[i]);

	// Spawn a new ball if there aren't very many
	if (Balls.numBalls < 10 && !Random.nextInt(0, 100)) {
		Balls.ball[Balls.numBalls++] = {x:Random.nextInt(20, Balls.canvasWidth*2), y:Random.nextInt(20, Balls.canvasHeight), radius: Random.nextInt(30,60), xVel:Random.nextFloat(-3,3), yVel:Random.nextFloat(-3,3)};
	}

	// Post message to draw balls
	self.postMessage(JSON.stringify([Balls.ball, Balls.numBalls]));
	setTimeout(moveBalls, fpsWait());
}

function fpsWait() {
	return 28;
}
